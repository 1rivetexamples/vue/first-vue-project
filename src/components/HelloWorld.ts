import { Component, Vue } from 'vue-property-decorator';
import WithRender from './hello-world.html';

import { SecondComponent } from './second.component';

@WithRender
@Component({
    components: {
        SecondComponent,
    },
})
export default class HelloWorld extends Vue {
    public msg: string = 'I am using TypeScript classes with Vue!';
}
