import { Vue, Component } from 'vue-property-decorator';
import withRender from './second.component.html';

@withRender
@Component
export class SecondComponent extends Vue {

}
